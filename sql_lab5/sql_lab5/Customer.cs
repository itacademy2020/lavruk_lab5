﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_lab5
{
    class Customer
    {
        const int TYPE_CUSTOMERS_LENGTH = 50;

        private string _customer_ID;
        private string _type_customers;

        public string Customer_ID
        {
            get
            {
                return _customer_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value))
                {
                    _customer_ID = null;
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _customer_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Customer_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public string Type_customers
        {
            get
            {
                return _type_customers;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _type_customers = null;
                }
                else if(value.Length < TYPE_CUSTOMERS_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if(stringBuilder[i] != ' ')
                            {
                                if (Char.IsLetter(stringBuilder[i]))
                                {
                                    isLetter = true;
                                }
                                else
                                {
                                    isLetter = false;
                                    break;
                                }
                            }
                        }

                        if (isLetter)
                        {
                            _type_customers = value;
                        }
                        else
                        {
                            throw new Exception("Тип особи повинен містити лише букви.");
                        }
                    }
                    else
                    {
                        _type_customers = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина типу особи більше {TYPE_CUSTOMERS_LENGTH} символів.");
                }
            }
        }

        public Customer()
        {
            Customer_ID = null;
            Type_customers = null;
        }

        public Customer(string type_customer)
        {
            Customer_ID = null;
            Type_customers = type_customer;
        }
    }
}