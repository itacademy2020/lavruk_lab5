﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_lab5
{
    class Manager
    {
        const int LINE_LENGTH = 50;
        const int MIN_AGE = 0;
        const int MAX_AGE = 200;
        const int MIN_EXPERIENCE = 0;

        private string _manager_ID;
        private string _name;
        private string _surname;
        private string _age;
        private string _experience;

        public string Manager_ID
        {
            get
            {
                return _manager_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value))
                {
                    _manager_ID = null;
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _manager_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Manager_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _name = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _name = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _name = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені менеджера більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Surname
        {
            get
            {
                return _surname;
            }
            set
            {

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _surname = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _surname = value;
                        }
                        else
                        {
                            throw new Exception("Прізвище повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _surname = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина прізвища менеджера більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Age
        {
            get
            {
                return _age;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _age = "NULL";
                }
                else
                {
                    if (!Int32.TryParse(value, out temp))
                    {
                        throw new Exception("Поле Age не вдалося привести до типу int.");
                    }

                    if (MAX_AGE < temp || temp < MIN_AGE)
                    {
                        throw new Exception("Невірний вік. Вік повинен бути більше 0.");
                    }
                    else
                    {
                        _age = value;
                    }
                }
            }
        }

        public string Experience
        {
            get
            {
                return _experience;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _experience = "NULL";
                }
                else
                {
                    if (!Int32.TryParse(value, out temp))
                    {
                        throw new Exception("Поле Experience не вдалося привести до типу int.");
                    }

                    if (Convert.ToInt32(Age) < temp || temp < MIN_EXPERIENCE)
                    {
                        throw new Exception("Невірний досвід. Досвід повинен бути більше 0 і не більше віку менеджера.");
                    }
                    else
                    {
                        _experience = value;
                    }
                }
            }
        }

        public Manager()
        {
            Manager_ID = null;
            Name = null;
            Surname = null;
            Age = null;
            Experience = null;
        }

        public Manager(string name, string surname, string age, string experience)
        {
            Manager_ID = null;
            Name = name;
            Surname = surname;
            Age = age;
            Experience = experience;
        }
    }
}