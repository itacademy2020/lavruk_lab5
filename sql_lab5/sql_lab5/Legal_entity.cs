﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_lab5
{
    class Legal_entity
    {
        const int LINE_LENGTH = 50;

        private string _legal_entity_ID;
        private string _name_organization;
        private string _state;
        private string _city;
        private string _street;
        private string _building;
        private string _type_customers_ID;

        public string Legal_entity_ID
        {
            get
            {
                return _legal_entity_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value))
                {
                    _legal_entity_ID = null;
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _legal_entity_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Legal_entity_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public string Name_organization
        {
            get
            {
                return _name_organization;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _name_organization = null;
                }
                else if(value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _name_organization = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я організації повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _name_organization = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені організації більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string State
        {
            get
            {
                return _state;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _state = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _state = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я області повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _state = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені області більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _city = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _city = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я міста повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _city = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені міста більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Street
        {
            get
            {
                return _street;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _street = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _street = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я вулиці повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _street = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені вулиці більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Building
        {
            get
            {
                return _building;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _building = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    _building = value;
                }
                else
                {
                    throw new Exception($"Довжина імені/номеру будинку більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Type_customers_ID
        {
            get
            {
                return _type_customers_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _type_customers_ID = "NULL";
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _type_customers_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Type_customers_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public Legal_entity()
        {
            Legal_entity_ID = null;
            Name_organization = null;
            State = null;
            City = null;
            Street = null;
            Building = null;
            Type_customers_ID = null;
        }

        public Legal_entity(string name_organization,
                            string state,
                            string city,
                            string street,
                            string building,
                            string type_customers_ID)
        {
            Legal_entity_ID = null;
            Name_organization = name_organization;
            State = state;
            City = city;
            Street = street;
            Building = building;
            Type_customers_ID = type_customers_ID;
        }
    }
}