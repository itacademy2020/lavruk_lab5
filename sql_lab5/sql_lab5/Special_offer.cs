﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_lab5
{
    class Special_offer
    {
        const byte min_discount = 0;
        const byte max_discount = 100;

        private string _special_offer_ID;
        private string _discount;
        private string _discount_start_date;
        private string _discount_expiration_date;

        public string Special_offer_ID
        {
            get
            {
                return _special_offer_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _special_offer_ID = null;
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _special_offer_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Special_offer_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public string Discount
        {
            get
            {
                return _discount;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _discount = "NULL";
                }
                else
                {
                    if (!Int32.TryParse(value, out temp))
                    {
                        throw new Exception("Поле Discount не вдалося привести до типу int.");
                    }

                    if (min_discount > temp || temp > max_discount)
                    {
                        throw new Exception("Невірна скидка. Скидка повинна бути в межах від 0 до 100");
                    }
                    else
                    {
                        _discount = value;
                    }
                }
            }
        }

        public string Discount_start_date
        {
            get
            {
                return _discount_start_date;
            }
            set
            {
                DateTime dateTime;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _discount_start_date = null;
                }
                else
                {
                    try
                    {
                        dateTime = Convert.ToDateTime(value);

                        _discount_start_date = dateTime.ToString("yyyy-MM-dd");
                    }
                    catch
                    {
                        throw new Exception("Поле Discount_start_date не вдалося привести до типу DateTime.");
                    }
                }
            }
        }

        public string Discount_expiration_date
        {
            get
            {
                return _discount_expiration_date;
            }
            set
            {
                DateTime dateTime;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _discount_expiration_date = null;
                }
                else
                {
                    try
                    {
                        dateTime = Convert.ToDateTime(value);

                        _discount_expiration_date = dateTime.ToString("yyyy-MM-dd");
                    }
                    catch
                    {
                        throw new Exception("Поле Discount_expiration_date не вдалося привести до типу DateTime.");
                    }
                }
            }
        }

        public Special_offer()
        {
            Special_offer_ID = null;
            Discount = null;
            Discount_start_date = null;
            Discount_expiration_date = null;
        }

        public Special_offer(string discount, string start_date, string end_date)
        {
            Special_offer_ID = null;
            Discount = discount;
            Discount_start_date = start_date;
            Discount_expiration_date = end_date;
        }
    }
}