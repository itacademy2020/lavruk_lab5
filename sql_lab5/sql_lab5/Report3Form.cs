﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    public partial class Report3Form : Form
    {
        Data_base DB = new Data_base();

        public Report3Form()
        {
            InitializeComponent();
        }

        private void Report3Form_Load(object sender, EventArgs e)
        {
            DB.FillCmbMonth(MonthComboBox);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DB.GenerateReport3(dgvAppRes, "Report3", MonthComboBox);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }
    }
}
