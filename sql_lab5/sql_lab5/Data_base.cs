﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    class Data_base
    {
        private string SQL_StringConnection = "Server=tcp:itacademy.database.windows.net,1433;"
                                     + "Database=Lavruk;"
                                     + "User Id=Lavruk;"
                                     + "Password=Lsuc93615;"
                                     + "Trusted_Connection=False;"
                                     + "Encrypt=True;";

        public void FillComboBoxSpecial_offer_ID(System.Windows.Forms.ComboBox cmb)
        {
            string sqlcommand = "SELECT * FROM [Special_offer]";

            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlCommand command = new SqlCommand(sqlcommand, sqlConnection);
            SqlDataReader sqlDataReader = command.ExecuteReader();
            cmb.Items.Clear();

            while (sqlDataReader.Read())
            {
                Special_offer special_offer = new Special_offer(sqlDataReader[1].ToString(), sqlDataReader[2].ToString(), sqlDataReader[3].ToString());

                special_offer.Special_offer_ID = sqlDataReader[0].ToString();

                cmb.Items.Add(sqlDataReader[0].ToString());
            }

            sqlDataReader.Close();
            sqlConnection.Close();
        }

        public void FillComboBoxSoftware_ID(System.Windows.Forms.ComboBox cmb)
        {
            string sqlcommand = "SELECT * FROM [Software]";

            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlCommand command = new SqlCommand(sqlcommand, sqlConnection);
            SqlDataReader sqlDataReader = command.ExecuteReader();
            cmb.Items.Clear();

            while (sqlDataReader.Read())
            {
                Software softwate = new Software(sqlDataReader[1].ToString(),
                                                 sqlDataReader[2].ToString(),
                                                 sqlDataReader[3].ToString(),
                                                 sqlDataReader[4].ToString());

                softwate.Software_ID = sqlDataReader[0].ToString();

                cmb.Items.Add(sqlDataReader[0].ToString());
            }

            sqlDataReader.Close();
            sqlConnection.Close();
        }

        public void FillComboBoxIndividual_ID(System.Windows.Forms.ComboBox cmb)
        {
            string sqlcommand = "SELECT * FROM [Individual]";

            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlCommand command = new SqlCommand(sqlcommand, sqlConnection);
            SqlDataReader sqlDataReader = command.ExecuteReader();
            cmb.Items.Clear();

            while (sqlDataReader.Read())
            {
                Individual individual = new Individual();

                individual.Individual_ID = sqlDataReader[0].ToString();

                cmb.Items.Add(sqlDataReader[0].ToString());
            }

            sqlDataReader.Close();
            sqlConnection.Close();
        }

        public void FillComboBoxLegal_entity_ID(System.Windows.Forms.ComboBox cmb)
        {
            string sqlcommand = "SELECT * FROM [Legal_entity]";

            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlCommand command = new SqlCommand(sqlcommand, sqlConnection);
            SqlDataReader sqlDataReader = command.ExecuteReader();
            cmb.Items.Clear();

            while (sqlDataReader.Read())
            {
                Legal_entity legal_Entity = new Legal_entity(sqlDataReader[1].ToString(),
                                                             sqlDataReader[2].ToString(),
                                                             sqlDataReader[3].ToString(),
                                                             sqlDataReader[4].ToString(),
                                                             sqlDataReader[5].ToString(),
                                                             sqlDataReader[6].ToString());

                legal_Entity.Legal_entity_ID = sqlDataReader[0].ToString();

                cmb.Items.Add(sqlDataReader[0].ToString());
            }

            sqlDataReader.Close();
            sqlConnection.Close();
        }

        public void FillComboBoxCustomer_ID(System.Windows.Forms.ComboBox cmb)
        {
            string sqlcommand = "SELECT * FROM [Customer]";

            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlCommand command = new SqlCommand(sqlcommand, sqlConnection);
            SqlDataReader sqlDataReader = command.ExecuteReader();
            cmb.Items.Clear();

            while (sqlDataReader.Read())
            {
                Customer customer = new Customer(sqlDataReader[1].ToString());

                customer.Customer_ID = sqlDataReader[0].ToString();

                cmb.Items.Add(sqlDataReader[0].ToString());
            }

            sqlDataReader.Close();
            sqlConnection.Close();
        }

        public void PrintTable(System.Windows.Forms.DataGridView dataGrid, string tableName, string command)
        {
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command, sqlConnection);
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, tableName);
            dataGrid.DataSource = dataSet.Tables[tableName];
            dataGrid.Refresh();

            sqlConnection.Close();
        }

        public void AddSoftware(string name, string provider, string price, string special_offer_ID)
        {
            Software software = new Software(name, provider, price, special_offer_ID);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            string command = $"INSERT iNTO [Software] ([name], [provider], [price], [special_offer_ID]) " +
                             $"VALUES ('{software.Name}', '{software.Provider}', {software.Price}, {software.Special_offer_ID})";

            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void UpdateSoftware(string name, string provider, string price, string special_offer_ID, System.Windows.Forms.ComboBox cmb)
        {
            Software software = new Software(name, provider, price, special_offer_ID);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);

            sqlConnection.Open();

            string mainCommand;
            string startCommand = "UPDATE [Software] SET";
            string endCommand;

            if (cmb.SelectedItem == null)
            {
                throw new Exception("Виберіть software_ID для оновлення.");
            }
            software.Software_ID = cmb.SelectedItem.ToString();
            endCommand = $" WHERE[software_ID] = {software.Software_ID}";

            string nameCommand = $"[name] = '{software.Name}'";
            string providerCommand = $"[provider] = '{software.Provider}'";
            string priceCommand = $"[price] = {software.Price}";
            string special_offerCommand = $"[special_offer_ID] = {software.Special_offer_ID}";

            if(!String.IsNullOrEmpty(name))
            {
                if(startCommand == "UPDATE [Software] SET")
                {
                    startCommand += nameCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += nameCommand;
                }
            }

            if (!String.IsNullOrEmpty(provider))
            {
                if (startCommand == "UPDATE [Software] SET")
                {
                    startCommand += providerCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += providerCommand;
                }
            }

            if (!String.IsNullOrEmpty(price))
            {
                if (startCommand == "UPDATE [Software] SET")
                {
                    startCommand += priceCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += priceCommand;
                }
            }

            if (!String.IsNullOrEmpty(special_offer_ID))
            {
                if (startCommand == "UPDATE [Software] SET")
                {
                    startCommand += special_offerCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += special_offerCommand;
                }
            }

            if(startCommand == "UPDATE [Software] SET")
            {
                throw new Exception("Введіть хоча б одне поле для оновлення.");
            }

            mainCommand = startCommand + endCommand;

            SqlCommand sqlCommand = new SqlCommand(mainCommand, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void AddSpecial_offer(string discount, string start_discount, string end_discount)
        {
            Special_offer special_Offer = new Special_offer(discount, start_discount, end_discount);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            string tstart_discount = String.IsNullOrEmpty(start_discount) ? "NULL" : $"'{special_Offer.Discount_start_date}'";
            string tend_discount = String.IsNullOrEmpty(end_discount) ? "NULL" : $"'{special_Offer.Discount_expiration_date}'";

            string command = $"INSERT iNTO [Special_offer] ([discount], [discount_start_date], [discount_expiratio_date])" +
                             $"VALUES ({special_Offer.Discount}," + tstart_discount + ", " + tend_discount + ")";

            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void UpdateSpecial_offer(string discount, string start_discount, string end_discount, System.Windows.Forms.ComboBox cmb)
        {
            Special_offer special_Offer = new Special_offer(discount, start_discount, end_discount);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);

            sqlConnection.Open();

            string mainCommand;
            string startCommand = "UPDATE [Special_offer] SET";
            string endCommand;

            if (cmb.SelectedItem == null)
            {
                throw new Exception("Виберіть Special_offer_ID для оновлення.");
            }
            special_Offer.Special_offer_ID = cmb.SelectedItem.ToString();
            endCommand = $" WHERE[special_offer_ID] = {special_Offer.Special_offer_ID}";

            string nameCommand = $"[discount] = {special_Offer.Discount}";
            string providerCommand = $"[discount_start_date] = '{special_Offer.Discount_start_date}'";
            string priceCommand = $"[discount_expiratio_date] = '{special_Offer.Discount_expiration_date}'";

            if (!String.IsNullOrEmpty(discount))
            {
                if (startCommand == "UPDATE [Special_offer] SET")
                {
                    startCommand += nameCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += nameCommand;
                }
            }

            if (!String.IsNullOrEmpty(start_discount))
            {
                if (startCommand == "UPDATE [Special_offer] SET")
                {
                    startCommand += providerCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += providerCommand;
                }
            }

            if (!String.IsNullOrEmpty(end_discount))
            {
                if (startCommand == "UPDATE [Special_offer] SET")
                {
                    startCommand += priceCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += priceCommand;
                }
            }

            if (startCommand == "UPDATE [Special_offer] SET")
            {
                throw new Exception("Введіть хоча б одне поле для оновлення.");
            }

            mainCommand = startCommand + endCommand;

            SqlCommand sqlCommand = new SqlCommand(mainCommand, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void AddIndividual(string name, string surname, string age,  string state, string city, string street, string building, string type_customers_ID)
        {
            Individual individual = new Individual(name, surname, age, state, city, street, building, type_customers_ID);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            string command = $"INSERT iNTO[Individual] ([name], [surname], [age], [state], [city], [street], [building], [type_customers_ID]) " +
                             $"VALUES ('{individual.Name}', " +
                             $"'{individual.Surname}', " +
                             $"{individual.Age}, " +
                             $"'{individual.State}', " +
                             $"'{individual.City}', " +
                             $"'{individual.Street}', " +
                             $"'{individual.Building}', " +
                             $"{individual.Type_customers_ID})";

            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void UpdateIndividual(string name, string surname, string age, string state, string city, string street, string building, string type_customers_ID, System.Windows.Forms.ComboBox cmb)
        {
            Individual individual = new Individual(name, surname, age, state, city, street, building, type_customers_ID);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);

            sqlConnection.Open();

            string mainCommand;
            string startCommand = "UPDATE [Individual] SET";
            string endCommand;

            if (cmb.SelectedItem == null)
            {
                throw new Exception("Виберіть Individual_ID для оновлення.");
            }
            individual.Individual_ID = cmb.SelectedItem.ToString();
            endCommand = $" WHERE[Individual_ID] = {individual.Individual_ID}";

            string nameCommand = $"[name] = '{individual.Name}'";
            string surnameCommand = $"[surname] = '{individual.Surname}'";
            string ageCommand = $"[age] = {individual.Age}";
            string stateCommand = $"[state] = '{individual.State}'";
            string cityCommand = $"[city] = '{individual.City}'";
            string streetCommand = $"[street] = '{individual.Street}'";
            string buildingCommand = $"[building] = '{individual.Building}'";
            string type_customers_IDCommand = $"[type_customers_ID] = {individual.Type_customers_ID}";

            if (!String.IsNullOrEmpty(name))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += nameCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += nameCommand;
                }
            }

            if (!String.IsNullOrEmpty(surname))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += surnameCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += surnameCommand;
                }
            }

            if (!String.IsNullOrEmpty(age))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += ageCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += ageCommand;
                }
            }

            if (!String.IsNullOrEmpty(state))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += stateCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += stateCommand;
                }
            }

            if (!String.IsNullOrEmpty(city))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += cityCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += cityCommand;
                }
            }

            if (!String.IsNullOrEmpty(street))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += streetCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += streetCommand;
                }
            }

            if (!String.IsNullOrEmpty(building))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += buildingCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += buildingCommand;
                }
            }

            if (!String.IsNullOrEmpty(type_customers_ID))
            {
                if (startCommand == "UPDATE [Individual] SET")
                {
                    startCommand += type_customers_IDCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += type_customers_IDCommand;
                }
            }

            if (startCommand == "UPDATE [Individual] SET")
            {
                throw new Exception("Введіть хоча б одне поле для оновлення.");
            }

            mainCommand = startCommand + endCommand;

            SqlCommand sqlCommand = new SqlCommand(mainCommand, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void AddLegal_entity(string name_organization, string state, string city, string street, string building, string type_customers_ID)
        {
            Legal_entity legal_Entity = new Legal_entity(name_organization, state, city, street, building, type_customers_ID);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            string command = $"INSERT iNTO[Legal_entity] ([name_organization], [state], [city], [street], [building], [type_customers_ID]) " +
                             $"VALUES ('{legal_Entity.Name_organization}', " +
                             $"'{legal_Entity.State}', " +
                             $"'{legal_Entity.City}', " +
                             $"'{legal_Entity.Street}', " +
                             $"'{legal_Entity.Building}', " +
                             $"{legal_Entity.Type_customers_ID})";

            SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void UpdateLegal_entity(string name_organization, string state, string city, string street, string building, string type_customers_ID, System.Windows.Forms.ComboBox cmb)
        {
            Legal_entity legal_Entity = new Legal_entity(name_organization, state, city, street, building, type_customers_ID);
            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);

            sqlConnection.Open();

            string mainCommand;
            string startCommand = "UPDATE [Legal_entity] SET";
            string endCommand;

            if (cmb.SelectedItem == null)
            {
                throw new Exception("Виберіть Legal_entity_ID для оновлення.");
            }
            legal_Entity.Legal_entity_ID = cmb.SelectedItem.ToString();
            endCommand = $" WHERE[Legal_entity_ID] = {legal_Entity.Legal_entity_ID}";

            string nameCommand = $"[name] = '{legal_Entity.Name_organization}'";
            string stateCommand = $"[state] = '{legal_Entity.State}'";
            string cityCommand = $"[city] = '{legal_Entity.City}'";
            string streetCommand = $"[street] = '{legal_Entity.Street}'";
            string buildingCommand = $"[building] = '{legal_Entity.Building}'";
            string type_customers_IDCommand = $"[type_customers_ID] = {legal_Entity.Type_customers_ID}";

            if (!String.IsNullOrEmpty(name_organization))
            {
                if (startCommand == "UPDATE [Legal_entity] SET")
                {
                    startCommand += nameCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += nameCommand;
                }
            }

            if (!String.IsNullOrEmpty(state))
            {
                if (startCommand == "UPDATE [Legal_entity] SET")
                {
                    startCommand += stateCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += stateCommand;
                }
            }

            if (!String.IsNullOrEmpty(city))
            {
                if (startCommand == "UPDATE [Legal_entity] SET")
                {
                    startCommand += cityCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += cityCommand;
                }
            }

            if (!String.IsNullOrEmpty(street))
            {
                if (startCommand == "UPDATE [Legal_entity] SET")
                {
                    startCommand += streetCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += streetCommand;
                }
            }

            if (!String.IsNullOrEmpty(building))
            {
                if (startCommand == "UPDATE [Legal_entity] SET")
                {
                    startCommand += buildingCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += buildingCommand;
                }
            }

            if (!String.IsNullOrEmpty(type_customers_ID))
            {
                if (startCommand == "UPDATE [Legal_entity] SET")
                {
                    startCommand += type_customers_IDCommand;
                }
                else
                {
                    startCommand += ",";
                    startCommand += type_customers_IDCommand;
                }
            }

            if (startCommand == "UPDATE [Legal_entity] SET")
            {
                throw new Exception("Введіть хоча б одне поле для оновлення.");
            }

            mainCommand = startCommand + endCommand;

            SqlCommand sqlCommand = new SqlCommand(mainCommand, sqlConnection);

            sqlCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void FillCmbNameSurnameManager(System.Windows.Forms.ComboBox cmb)
        {
            string sqlcommand = "SELECT * FROM [Manager]";

            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlCommand command = new SqlCommand(sqlcommand, sqlConnection);
            SqlDataReader sqlDataReader = command.ExecuteReader();
            cmb.Items.Clear();

            while (sqlDataReader.Read())
            {
                Manager manager = new Manager(sqlDataReader[1].ToString(),
                                              sqlDataReader[2].ToString(),
                                              sqlDataReader[3].ToString(),
                                              sqlDataReader[4].ToString());

                manager.Manager_ID = sqlDataReader[0].ToString();

                cmb.Items.Add($"{sqlDataReader[1].ToString()} {sqlDataReader[2].ToString()}");
            }

            sqlDataReader.Close();
            sqlConnection.Close();
        }

        public void FillCmbNameSoftware(System.Windows.Forms.ComboBox cmb)
        {
            string sqlcommand = "SELECT * FROM [Software]";

            SqlConnection sqlConnection = new SqlConnection(SQL_StringConnection);
            sqlConnection.Open();

            SqlCommand command = new SqlCommand(sqlcommand, sqlConnection);
            SqlDataReader sqlDataReader = command.ExecuteReader();
            cmb.Items.Clear();

            while (sqlDataReader.Read())
            {
                Software software = new Software(sqlDataReader[1].ToString(),
                                              sqlDataReader[2].ToString(),
                                              sqlDataReader[3].ToString(),
                                              sqlDataReader[4].ToString());

                software.Software_ID = sqlDataReader[0].ToString();

                cmb.Items.Add($"{sqlDataReader[1]}");
            }

            sqlDataReader.Close();
            sqlConnection.Close();
        }

        public void GenerateReport1(System.Windows.Forms.DataGridView dataGrid, 
                                    string tableName, 
                                    System.Windows.Forms.ComboBox cmb1, 
                                    System.Windows.Forms.ComboBox cmb2)
        {
            if (cmb1.SelectedItem == null)
            {
                throw new Exception("Виберіть менеджера.");
            }

            if (cmb2.SelectedItem == null)
            {
                throw new Exception("Виберіть програмне забезпечення.");
            }

            string tname = cmb1.Text.ToString().Split(' ')[0];
            string tsurname = cmb1.Text.ToString().Split(' ')[1];

            string command = "SELECT [Manager].[name],[Manager].[surname], " +
                                    "[Software].[name],[Application].[date_of_purchase], " +
                                    "[Application].[total_price]" +
                             "FROM[Software] JOIN[Software_and_application]" +
                             "ON[Software].[software_ID] = [Software_and_application].[software_ID]" +
                             "JOIN[Application]" +
                             "ON[Software_and_application].[application_ID] = [Application].[application_ID]" +
                             "JOIN[Manager]" +
                             "ON[Manager].[manager_ID] = [Application].[manager_ID]" +
                             $"WHERE([Manager].[name] = '{tname}'" +
                             $"AND[Manager].[surname] = '{tsurname}'" +
                             $"AND[Software].[name] = '{cmb2.Text.ToString()}')";
            
            PrintTable(dataGrid, tableName, command);
        }

        public void FillCmbMonth(System.Windows.Forms.ComboBox cmb)
        {
            const int MONTH = 12;

            for(int i = 1; i < MONTH + 1; i++)
            {
                cmb.Items.Add(i.ToString());
            }
        }

        public void GenerateReport2(System.Windows.Forms.DataGridView dataGrid,
                                    string tableName,
                                    System.Windows.Forms.TextBox textBox)
        {
            if (String.IsNullOrEmpty(textBox.Text))
            {
                throw new Exception("Введіть назву міста.");
            }

            string command = "SELECT [Customer].[type_customers]," +
                                    "[Legal_entity].[name_organization]," +
                                    "[Legal_entity].[city]," +
                                    "[Legal_entity].[street]," +
                                    "[Legal_entity].[building]," +
                                    "[Application].[date_of_purchase]," +
                                    "[Application].[total_price]" +
                             "FROM[Customer] JOIN[Legal_entity]" +
                             "ON[Customer].[customer_ID] = [Legal_entity].[type_customers_ID]" +
                             "JOIN[Application]" +
                             " ON[Application].[customer_ID] = [Customer].[customer_ID]" +
                             $"WHERE([Legal_entity].[city] = '{textBox.Text.ToString()}')";

            PrintTable(dataGrid, tableName, command);
        }

        public void GenerateReport3(System.Windows.Forms.DataGridView dataGrid,
                                    string tableName,
                                    System.Windows.Forms.ComboBox cmb)
        {
            if (cmb.SelectedItem == null)
            {
                throw new Exception("Виберіть місяць.");
            }

            DateTime dateTime = new DateTime(1900, Convert.ToInt32(cmb.SelectedItem), 01);

            string command = "SELECT * FROM Software WHERE software_ID NOT IN (SELECT s.software_ID" +
                             " FROM[Software] s JOIN[Software_and_application]" +
                             " ON[Software].[software_ID] = [Software_and_application].[software_ID]" +
                             " JOIN[Application] ON[Software_and_application].[application_ID] = [Application].[application_ID]" +
                             $" WHERE MONTH([Application].[date_of_purchase]) = MONTH('{dateTime.ToString("yyyy-MM-dd")}') AND" +
                             " YEAR([Application].[date_of_purchase]) = YEAR(GETDATE()))";

            PrintTable(dataGrid, tableName, command);
        }
    }
}