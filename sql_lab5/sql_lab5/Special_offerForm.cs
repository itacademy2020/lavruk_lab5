﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    public partial class Special_offerForm : Form
    {
        Data_base DB = new Data_base();

        public Special_offerForm()
        {
            InitializeComponent();
        }

        private void Special_offerForm_Load(object sender, EventArgs e)
        {
            DB.FillComboBoxSpecial_offer_ID(Special_offerComboBox);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DB.AddSpecial_offer(DiscountBox.Text.ToString(), dateStartBox.Text.ToString(), dateEndBox.Text.ToString());

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sqlcommand = "SELECT * FROM [Special_offer]";
            DB.PrintTable(dgvAppRes, "Special_offer", sqlcommand);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                DB.UpdateSpecial_offer(DiscountBox.Text.ToString(), dateStartBox.Text.ToString(), dateEndBox.Text.ToString(), Special_offerComboBox);

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }
    }
}