﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    public partial class Report1Form : Form
    {
        Data_base DB = new Data_base();

        public Report1Form()
        {
            InitializeComponent();
        }

        private void Report1Form_Load(object sender, EventArgs e)
        {
            DB.FillCmbNameSurnameManager(NameSurnameManagerComboBox);
            DB.FillCmbNameSoftware(NameSoftwareComboBox);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DB.GenerateReport1(dgvAppRes, "Report1", NameSurnameManagerComboBox, NameSoftwareComboBox);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }
        
    }
}
