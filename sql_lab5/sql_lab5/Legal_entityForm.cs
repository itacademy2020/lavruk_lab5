﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    public partial class Legal_entityForm : Form
    {
        Data_base DB = new Data_base();

        public Legal_entityForm()
        {
            InitializeComponent();
        }

        private void Legal_entityForm_Load(object sender, EventArgs e)
        {
            DB.FillComboBoxLegal_entity_ID(Legal_entity_IDComboBox);
            DB.FillComboBoxCustomer_ID(Customer_IDComboBox);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DB.AddLegal_entity(Name_organizationBox.Text.ToString(),
                                   StateBox.Text.ToString(),
                                   CityBox.Text.ToString(),
                                   StreetBox.Text.ToString(),
                                   BuildingBox.Text.ToString(),
                                   Customer_IDComboBox.Text.ToString());

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sqlcommand = "SELECT * FROM [Legal_entity]";
            DB.PrintTable(dgvAppRes, "Legal_entity", sqlcommand);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                DB.UpdateLegal_entity(Name_organizationBox.Text.ToString(),
                                      StateBox.Text.ToString(),
                                      CityBox.Text.ToString(),
                                      StreetBox.Text.ToString(),
                                      BuildingBox.Text.ToString(),
                                      Customer_IDComboBox.Text.ToString(),
                                      Legal_entity_IDComboBox);

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }
    }
}
