﻿namespace sql_lab5
{
    partial class Legal_entityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.CityBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.StateBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Customer_IDComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.StreetBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Legal_entity_IDComboBox = new System.Windows.Forms.ComboBox();
            this.BuildingBox = new System.Windows.Forms.TextBox();
            this.Name_organizationBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvAppRes = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAppRes)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(138, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Legal_entity_ID (for update)";
            // 
            // CityBox
            // 
            this.CityBox.Location = new System.Drawing.Point(11, 137);
            this.CityBox.Name = "CityBox";
            this.CityBox.Size = new System.Drawing.Size(184, 20);
            this.CityBox.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "City";
            // 
            // StateBox
            // 
            this.StateBox.Location = new System.Drawing.Point(11, 86);
            this.StateBox.Name = "StateBox";
            this.StateBox.Size = new System.Drawing.Size(184, 20);
            this.StateBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "State";
            // 
            // Customer_IDComboBox
            // 
            this.Customer_IDComboBox.FormattingEnabled = true;
            this.Customer_IDComboBox.Location = new System.Drawing.Point(10, 286);
            this.Customer_IDComboBox.Name = "Customer_IDComboBox";
            this.Customer_IDComboBox.Size = new System.Drawing.Size(185, 21);
            this.Customer_IDComboBox.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 269);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Customers_ID";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(24, 467);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(184, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Change data";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(23, 438);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Show Table";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Add new legal entity";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 221);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Building";
            // 
            // StreetBox
            // 
            this.StreetBox.Location = new System.Drawing.Point(11, 187);
            this.StreetBox.Name = "StreetBox";
            this.StreetBox.Size = new System.Drawing.Size(184, 20);
            this.StreetBox.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 171);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Street";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Legal_entity_IDComboBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.BuildingBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.StreetBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.CityBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.StateBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Customer_IDComboBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Name_organizationBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 368);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter data";
            // 
            // Legal_entity_IDComboBox
            // 
            this.Legal_entity_IDComboBox.FormattingEnabled = true;
            this.Legal_entity_IDComboBox.Location = new System.Drawing.Point(10, 336);
            this.Legal_entity_IDComboBox.Name = "Legal_entity_IDComboBox";
            this.Legal_entity_IDComboBox.Size = new System.Drawing.Size(185, 21);
            this.Legal_entity_IDComboBox.TabIndex = 31;
            // 
            // BuildingBox
            // 
            this.BuildingBox.Location = new System.Drawing.Point(11, 237);
            this.BuildingBox.Name = "BuildingBox";
            this.BuildingBox.Size = new System.Drawing.Size(184, 20);
            this.BuildingBox.TabIndex = 29;
            // 
            // Name_organizationBox
            // 
            this.Name_organizationBox.Location = new System.Drawing.Point(10, 37);
            this.Name_organizationBox.Name = "Name_organizationBox";
            this.Name_organizationBox.Size = new System.Drawing.Size(184, 20);
            this.Name_organizationBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name_orgainzation";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button3);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvAppRes);
            this.splitContainer1.Size = new System.Drawing.Size(1163, 525);
            this.splitContainer1.SplitterDistance = 224;
            this.splitContainer1.TabIndex = 3;
            // 
            // dgvAppRes
            // 
            this.dgvAppRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAppRes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAppRes.Location = new System.Drawing.Point(0, 0);
            this.dgvAppRes.Name = "dgvAppRes";
            this.dgvAppRes.Size = new System.Drawing.Size(935, 525);
            this.dgvAppRes.TabIndex = 0;
            // 
            // Legal_entityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 525);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Legal_entityForm";
            this.Text = "Legal_entityForm";
            this.Load += new System.EventHandler(this.Legal_entityForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAppRes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox CityBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox StateBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Customer_IDComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox StreetBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox Legal_entity_IDComboBox;
        private System.Windows.Forms.TextBox BuildingBox;
        private System.Windows.Forms.TextBox Name_organizationBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvAppRes;
    }
}