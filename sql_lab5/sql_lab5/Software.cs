﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_lab5
{
    class Software
    {
        const int NAME_LENGTH = 30;
        const int PROVIDER_LENGTH = 50;

        private string _software_ID;
        private string _name;
        private string _provider;
        private string _price;
        private string _special_offer_ID;

        public string Software_ID
        {
            get
            {
                return _software_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _software_ID = null;
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _software_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Software_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public string Name
         {
            get
            {
                return _name;
            }
            set
            {
                if(value == null || value == "NULL")
                {
                    _name = null;
                }
                else if (value.Length < NAME_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for(int i = 0; i < value.Length; i++)
                        {
                            if(Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                        }

                        if(isLetter)
                        {
                            _name = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я продукту поминно містити хоча б одну букву.");
                        }
                    }
                    else
                    {
                        _name = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені продукту більше {NAME_LENGTH} символів.");
                }
            }
         }

        public string Provider
        {
            get
            {
                return _provider;
            }
            set
            {
                if (value == null || value == "NULL")
                {
                    _provider = null;
                }
                else if (value.Length < PROVIDER_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                        }

                        if (isLetter)
                        {
                            _provider = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я постачальника поминно містити хоча б одну букву.");
                        }
                    }
                    else
                    {
                        _provider = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені постачальника більше {PROVIDER_LENGTH} символів.");
                }
            }
        }

        public string Price
        {
            get
            {
                return _price;
            }
            set
            {
                float temp;

                if(String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _price = "NULL";
                }
                else
                {
                    if (!float.TryParse(value, out temp))
                    {
                        throw new Exception("Поле Price не вдалося привести до типу float.");
                    }

                    if (temp < 0 || temp < float.MinValue || temp > float.MaxValue)
                    {
                        throw new Exception("Невірна ціна. Ціна повинна бути більше 0.");
                    }
                    else
                    {
                        _price = value;
                    }
                }
            }
        }

        public string Special_offer_ID
        {
            get
            {
                return _special_offer_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _special_offer_ID = "NULL";
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _special_offer_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Special_offer_ID не вдалося привести до типу int.");
                    }
                } 
            }
        }

        public Software()
        {
            Software_ID = null;
            Name = null;
            Provider = null;
            Price = null;
            Special_offer_ID = null;
        }

        public Software(string name, string provider, string price, string special_offer_ID)
        {
            Software_ID = null;
            Name = name;
            Provider = provider;
            Price = price;
            Special_offer_ID = special_offer_ID;
        }
    }
}