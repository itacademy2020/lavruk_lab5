﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    public partial class MainMenuForm : Form
    {
        public MainMenuForm()
        {
            InitializeComponent();
        }

        private void Software_button_Click(object sender, EventArgs e)
        {
            SoftwareForm softwareForm = new SoftwareForm();
            softwareForm.Show();
        }

        private void Special_offer_button_Click(object sender, EventArgs e)
        {
            Special_offerForm special_OfferForm = new Special_offerForm();
            special_OfferForm.Show();
        }

        private void Individual_button_Click(object sender, EventArgs e)
        {
            IndividualForm individualForm = new IndividualForm();
            individualForm.Show();
        }

        private void Legal_entity_button_Click(object sender, EventArgs e)
        {
            Legal_entityForm legal_EntityForm = new Legal_entityForm();
            legal_EntityForm.Show();
        }

        private void Report1_button_Click(object sender, EventArgs e)
        {
            Report1Form report1Form = new Report1Form();
            report1Form.Show();
        }

        private void Report2_button_Click(object sender, EventArgs e)
        {
            Report2Form report2Form = new Report2Form();
            report2Form.Show();
        }

        private void Report3_button_Click(object sender, EventArgs e)
        {
            Report3Form report3Form = new Report3Form();
            report3Form.Show();
        }
    }
}
