﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    public partial class IndividualForm : Form
    {
        Data_base DB = new Data_base();

        public IndividualForm()
        {
            InitializeComponent();
        }

        private void IndividualForm_Load(object sender, EventArgs e)
        {
            DB.FillComboBoxIndividual_ID(Individual_IDComboBox);
            DB.FillComboBoxCustomer_ID(Customer_IDComboBox);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DB.AddIndividual(NameBox.Text.ToString(),
                                 SurnameBox.Text.ToString(), 
                                 AgeBox.Text.ToString(), 
                                 StateBox.Text.ToString(),
                                 CityBox.Text.ToString(),
                                 StreetBox.Text.ToString(), 
                                 BuildingBox.Text.ToString(),
                                 Customer_IDComboBox.Text.ToString());

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sqlcommand = "SELECT * FROM [Individual]";
            DB.PrintTable(dgvAppRes, "Individual", sqlcommand);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                DB.UpdateIndividual(NameBox.Text.ToString(),
                                    SurnameBox.Text.ToString(), 
                                    AgeBox.Text.ToString(), 
                                    StateBox.Text.ToString(),
                                    CityBox.Text.ToString(),
                                    StreetBox.Text.ToString(), 
                                    BuildingBox.Text.ToString(),
                                    Customer_IDComboBox.Text.ToString(),
                                    Individual_IDComboBox);

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }
    }
}