﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sql_lab5
{
    public partial class SoftwareForm : Form
    {
        Data_base DB = new Data_base();

        public SoftwareForm()
        {
            InitializeComponent();
        }

        private void SoftwareForm_Load(object sender, EventArgs e)
        {
            DB.FillComboBoxSpecial_offer_ID(cmbSpecial_offer_ID);
            DB.FillComboBoxSoftware_ID(comboBox1);
    }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DB.AddSoftware(NameTextBox.Text.ToString(), ProviderTextBox.Text.ToString(), PriceTextBox.Text.ToString(), cmbSpecial_offer_ID.Text.ToString());

                MessageBox.Show("Successfully.");
            }
            catch(Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sqlcommand = "SELECT * FROM [Software]";
            DB.PrintTable(dgvAppRes, "Software", sqlcommand);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                DB.UpdateSoftware(NameTextBox.Text.ToString(), ProviderTextBox.Text.ToString(), PriceTextBox.Text.ToString(), cmbSpecial_offer_ID.Text.ToString(), comboBox1);

                MessageBox.Show("Successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error: {ex.Message}");
            }
        }
    }
}