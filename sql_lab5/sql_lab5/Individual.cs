﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql_lab5
{
    class Individual
    {
        const int LINE_LENGTH = 50;
        const int MIN_AGE = 0;
        const int MAX_AGE = 200;

        private string _individual_ID;
        private string _name;
        private string _surname;
        private string _age;
        private string _state;
        private string _city;
        private string _street;
        private string _building;
        private string _type_customers_ID;

        public string Individual_ID
        {
            get
            {
                return _individual_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value))
                {
                    _individual_ID = null;
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _individual_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Individual_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _name = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _name = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _name = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені фізичної особи більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Surname
        {
            get
            {
                return _surname;
            }
            set
            {

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _surname = null;
                }
                else if (value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _surname = value;
                        }
                        else
                        {
                            throw new Exception("Прізвище повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _surname = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина прізвища фізичної особи більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Age
        {
            get
            {
                return _age;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _age = "NULL";
                }
                else
                {
                    if (!Int32.TryParse(value, out temp))
                    {
                        throw new Exception("Поле Age не вдалося привести до типу int.");
                    }

                    if (MAX_AGE < temp || temp < MIN_AGE)
                    {
                        throw new Exception("Невірний вік. Вік повинен бути більше 0.");
                    }
                    else
                    {
                        _age = value;
                    }
                }
            }
        }

        public string State
        {
            get
            {
                return _state;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _state = null;
                }
                else if(value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _state = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я області повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _state = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені області більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _city = null;
                }
                else if(value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _city = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я міста повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _city = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені міста більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Street
        {
            get
            {
                return _street;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _street = null;
                }
                else if(value.Length < LINE_LENGTH)
                {
                    if (value.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(value);
                        bool isLetter = false;

                        for (int i = 0; i < value.Length; i++)
                        {
                            if (Char.IsLetter(stringBuilder[i]))
                            {
                                isLetter = true;
                            }
                            else
                            {
                                isLetter = false;
                                break;
                            }
                        }

                        if (isLetter)
                        {
                            _street = value;
                        }
                        else
                        {
                            throw new Exception("Ім'я вулиці повинно містити лише букви.");
                        }
                    }
                    else
                    {
                        _street = value;
                    }
                }
                else
                {
                    throw new Exception($"Довжина імені вулиці більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Building
        {
            get
            {
                return _building;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _building = null;
                }
                else if(value.Length < LINE_LENGTH)
                {
                    _building = value;
                }
                else
                {
                    throw new Exception($"Довжина імені/номеру будинку більше {LINE_LENGTH} символів.");
                }
            }
        }

        public string Type_customers_ID
        {
            get
            {
                return _type_customers_ID;
            }
            set
            {
                int temp;

                if (String.IsNullOrEmpty(value) || value == "NULL")
                {
                    _type_customers_ID = "NULL";
                }
                else
                {
                    try
                    {
                        temp = Convert.ToInt32(value);

                        if (temp < 1)
                        {
                            throw new Exception("Невірне ID.");
                        }
                        else
                        {
                            _type_customers_ID = value;
                        }
                    }
                    catch
                    {
                        throw new Exception("Поле Type_customers_ID не вдалося привести до типу int.");
                    }
                }
            }
        }

        public Individual()
        {
            Individual_ID = null;
            Name = null;
            Surname = null;
            Age = null;
            State = null;
            City = null;
            Street = null;
            Building = null;
            Type_customers_ID = null;
        }

        public Individual(string name, 
                          string surname, 
                          string age, 
                          string state, 
                          string city, 
                          string street, 
                          string building, 
                          string type_customers_ID)
        {
            Individual_ID = null;
            Name = name;
            Surname = surname;
            Age = age;
            State = state;
            City = city;
            Street = street;
            Building = building;
            Type_customers_ID = type_customers_ID;
        }
    }
}