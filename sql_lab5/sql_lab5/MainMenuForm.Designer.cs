﻿namespace sql_lab5
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Report3_button = new System.Windows.Forms.Button();
            this.Report2_button = new System.Windows.Forms.Button();
            this.Report1_button = new System.Windows.Forms.Button();
            this.Legal_entity_button = new System.Windows.Forms.Button();
            this.Individual_button = new System.Windows.Forms.Button();
            this.Special_offer_button = new System.Windows.Forms.Button();
            this.Software_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label3.Location = new System.Drawing.Point(251, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Report generation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label2.Location = new System.Drawing.Point(25, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 17);
            this.label2.TabIndex = 18;
            this.label2.Text = "Operations with tables";
            // 
            // Report3_button
            // 
            this.Report3_button.Location = new System.Drawing.Point(233, 173);
            this.Report3_button.Name = "Report3_button";
            this.Report3_button.Size = new System.Drawing.Size(153, 23);
            this.Report3_button.TabIndex = 17;
            this.Report3_button.Text = "Generate report №3";
            this.Report3_button.UseVisualStyleBackColor = true;
            this.Report3_button.Click += new System.EventHandler(this.Report3_button_Click);
            // 
            // Report2_button
            // 
            this.Report2_button.Location = new System.Drawing.Point(233, 135);
            this.Report2_button.Name = "Report2_button";
            this.Report2_button.Size = new System.Drawing.Size(153, 23);
            this.Report2_button.TabIndex = 16;
            this.Report2_button.Text = "Generate report №2";
            this.Report2_button.UseVisualStyleBackColor = true;
            this.Report2_button.Click += new System.EventHandler(this.Report2_button_Click);
            // 
            // Report1_button
            // 
            this.Report1_button.Location = new System.Drawing.Point(233, 96);
            this.Report1_button.Name = "Report1_button";
            this.Report1_button.Size = new System.Drawing.Size(153, 23);
            this.Report1_button.TabIndex = 15;
            this.Report1_button.Text = "Generate report №1";
            this.Report1_button.UseVisualStyleBackColor = true;
            this.Report1_button.Click += new System.EventHandler(this.Report1_button_Click);
            // 
            // Legal_entity_button
            // 
            this.Legal_entity_button.Location = new System.Drawing.Point(20, 219);
            this.Legal_entity_button.Name = "Legal_entity_button";
            this.Legal_entity_button.Size = new System.Drawing.Size(153, 23);
            this.Legal_entity_button.TabIndex = 14;
            this.Legal_entity_button.Text = "Legal entity";
            this.Legal_entity_button.UseVisualStyleBackColor = true;
            this.Legal_entity_button.Click += new System.EventHandler(this.Legal_entity_button_Click);
            // 
            // Individual_button
            // 
            this.Individual_button.Location = new System.Drawing.Point(20, 179);
            this.Individual_button.Name = "Individual_button";
            this.Individual_button.Size = new System.Drawing.Size(153, 23);
            this.Individual_button.TabIndex = 13;
            this.Individual_button.Text = "Individual";
            this.Individual_button.UseVisualStyleBackColor = true;
            this.Individual_button.Click += new System.EventHandler(this.Individual_button_Click);
            // 
            // Special_offer_button
            // 
            this.Special_offer_button.Location = new System.Drawing.Point(20, 137);
            this.Special_offer_button.Name = "Special_offer_button";
            this.Special_offer_button.Size = new System.Drawing.Size(153, 23);
            this.Special_offer_button.TabIndex = 12;
            this.Special_offer_button.Text = "Special offer";
            this.Special_offer_button.UseVisualStyleBackColor = true;
            this.Special_offer_button.Click += new System.EventHandler(this.Special_offer_button_Click);
            // 
            // Software_button
            // 
            this.Software_button.Location = new System.Drawing.Point(20, 96);
            this.Software_button.Name = "Software_button";
            this.Software_button.Size = new System.Drawing.Size(153, 23);
            this.Software_button.TabIndex = 11;
            this.Software_button.Text = "Software";
            this.Software_button.UseVisualStyleBackColor = true;
            this.Software_button.Click += new System.EventHandler(this.Software_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.label1.Location = new System.Drawing.Point(158, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Main menu";
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 257);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Report3_button);
            this.Controls.Add(this.Report2_button);
            this.Controls.Add(this.Report1_button);
            this.Controls.Add(this.Legal_entity_button);
            this.Controls.Add(this.Individual_button);
            this.Controls.Add(this.Special_offer_button);
            this.Controls.Add(this.Software_button);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainMenuForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Report3_button;
        private System.Windows.Forms.Button Report2_button;
        private System.Windows.Forms.Button Report1_button;
        private System.Windows.Forms.Button Legal_entity_button;
        private System.Windows.Forms.Button Individual_button;
        private System.Windows.Forms.Button Special_offer_button;
        private System.Windows.Forms.Button Software_button;
        private System.Windows.Forms.Label label1;
    }
}

